package com.example.mariamagdalena.course2;

public class Problem5 {
    public static void main(String[] args)
    {
        printSubstring("development", 1,-2);
    }

    static void printSubstring(String s, int start, int end)
    {
        if(start > end)
        {
            System.out.println("Start is bigger than end");
            return;
        }
        if(start < 0)
        {
            System.out.println("Start shouldn't be negative");
            return;
        }
        if(end < 0)
        {
            System.out.println("End shouldn't be negative");
            return;
        }
        if( end > s.length() )
        {
            System.out.println("End is bigger than length of string");
            return;
        }
        if(start == end)
        {
            System.out.println("Start = end.");
            return;
        }
        System.out.println(s.substring(start,end));
    }
}
