package com.example.mariamagdalena.course2;

public class Problem2 {
    public static void main(String[] args)
    {
        int result2 = calculateDigits(235230);
        System.out.println("Sum of digits is: " + result2);
    }
    static int calculateDigits(int a)
    {
        int sum = 0;
        while (a != 0)
        {
            sum = sum + a%10;
            a = a/10;
        }
        return sum;
    }
}
