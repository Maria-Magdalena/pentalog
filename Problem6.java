package com.example.mariamagdalena.course2;

import java.sql.SQLOutput;

public class Problem6 {
    public static void main(String[] args)
    {
        createPattern(5);
    }

    static void createPattern(int n)
    {
        int  k = 0;

        for(int i = 1; i <= n; ++i, k = 0) {
            for(int space = 1; space <= n - i; ++space) {
                System.out.print("=");
            }
            System.out.print("/");
            while(k != 2 * i - 1) {
                System.out.print("_");
                ++k;
            }
            System.out.print("\\");
            for(int space = 1; space <= n - i; ++space) {
                System.out.print("=");
            }
            System.out.println();
        }
    }
}
