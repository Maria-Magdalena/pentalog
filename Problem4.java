package com.example.mariamagdalena.course2;

public class Problem4 {
    public static void main(String[] args)
    {
         System.out.println(verifyPalindrom("a"));
    }
    static String verifyPalindrom(String a)
    {
        int n = a.length();
        for(int i = 0; i< n/2; i++)
        {
            if (a.charAt(i) != a.charAt(n - i - 1))
                return "No";
        }
        return "Yes";
    }
}
